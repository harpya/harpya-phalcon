# Routing Methods

There are 3 ways to interact with "external world", in a Controller class:

- Just return array
- Controller's request/response objects
- Method's parameters (request, response and vars)




## 1. Just return array

Simplest way. Just return an array with contents you want return to browser.
The framework will intercept this data, convert to JSON format, and send it. 
The Status code should always be 200, except when some error/exception is thrown. In this case, should be 400.
If you want modify any header, use the PHP's native ``header`` command to do so. 

## 2. Controller's request/response objects

Each Controller object have it's own ``request`` and ``response`` objects, filled with original HTTP request. 
You can use it, and have all power to change headers, set status code, and so on.
Great to  send binary files, for example.  

## 3. Method's parameters 

The framework will invoke the Controller's method passing 3 parameters:
- request
- response (by reference)
- variables (array with parameters)

