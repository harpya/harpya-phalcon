<?php

namespace harpya\phalcon;

/**
 * Trait ConfigLoader
 * @package harpya\ca
 */
trait ConfigLoader
{
    protected $config;

    /**
     * @param $filename
     */
    protected function loadConfig($filename)
    {
        $this->getConfig()->loadFromFile($filename);
    }

    /**
     * @return \harpya\config_manager\ConfigManager
     */
    public function getConfig() : \harpya\config_manager\ConfigManager
    {
        if (!$this->config) {
            $this->config = new \harpya\config_manager\ConfigManager();
        }

        return $this->config;
    }

    /**
     * @param \harpya\config_manager\ConfigManager $config
     */
    public function setConfig(\harpya\config_manager\ConfigManager $config) {
        $this->config = $config;
    }
}
